    <ul>
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Groups'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Contacts'), ['controller' => 'Contacts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Contact'), ['controller' => 'Contacts', 'action' => 'add']) ?></li>
    </ul>
    
    <?php echo $this->Form->create($group, ['align' => [
    'sm' => [
        'left' => 6,
        'middle' => 6,
        'right' => 12
    ],
    'md' => [
        'left' => 4,
        'middle' => 4,
        'right' => 4
    ]
]]) ?>
    <fieldset>
        <legend><?= __('Adicionar Grupo') ?></legend>
        <?php
            echo $this->Form->input('nome', ['label'=>'Nome:']);
        ?>
    </fieldset>
    <div class="form-group submit">
	<div class="col-sm-offset-6 col-md-offset-4 col-sm-6 col-md-4">
      <?php 
      		echo $this->Form->button('Salvar Grupo', array('class'=>'btn btn-primary'));
      		echo $this->Html->link('Cancelar', array('action'=>'index'),array('class'=>'btn btn-danger'));
      ?>
    </div>
</div>
    <?php echo $this->Form->end() ?>