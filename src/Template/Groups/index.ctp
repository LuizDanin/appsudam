<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Group'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Contacts'), ['controller' => 'Contacts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Contact'), ['controller' => 'Contacts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="groups index large-9 medium-8 columns content">
    <h3><?= __('Grupos') ?></h3>
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($groups as $group): ?>
            <tr>
                <td><?php echo $this->Number->format($group->id) ?></td>
                <td><?php echo h($group->nome) ?></td>
                <td class="actions">
                <?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span> Detalhe', ['action' => 'view', $group->id], ['escape' => false, 'class'=>'btn btn-xs btn-primary']) ?>
                <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', ['action' => 'edit', $group->id], ['escape' => false, 'class' => 'btn btn-xs btn-success']) ?>    
                <?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span> Excluir'), ['action' => 'delete', $group->id], ['class' => 'btn btn-xs btn-danger','escape' => false, 'confirm' => __('Deseja Excluir este Grupo # {0}?', $group->id)] ); ?>    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
