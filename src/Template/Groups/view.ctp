<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Group'), ['action' => 'edit', $group->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Group'), ['action' => 'delete', $group->id], ['confirm' => __('Are you sure you want to delete # {0}?', $group->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Groups'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Group'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Contacts'), ['controller' => 'Contacts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contact'), ['controller' => 'Contacts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<h2>Detalhes do Grupo</h2>
<div class="groups view large-9 medium-8 columns content">
    <h3><?php echo h($group->nome) ?></h3>
    <table class="table table-no-border">
        <tr>
            <th><?= __('Nome:') ?></th>
            <td><?= h($group->nome) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Contatos Relacionados:') ?></h4>
        <?php if (!empty($group->contacts)): ?>
        <table class="table">
            <tr>
                <th><?= __('Nome') ?></th>
                <th><?= __('Instituicao') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Telefone') ?></th>
                <th class="actions"><?= __('Ação') ?></th>
            </tr>
        <?php
        	
           //@TODO arrayColorLines deixar dinamico, está GoHorse
           $arrayColorLines = ['success','info','warning','active','success','info','warning','active','success','info','warning','active'];
           $i=0;
            foreach ($group->contacts as $contacts): ?>
            
            <tr class="<?php echo $arrayColorLines[$i];?>">
                <td><?php echo h($contacts->nome) ?></td>
                <td><?php echo h($contacts->instituicao) ?></td>
                <td><?php echo h($contacts->email) ?></td>
                <td><?php echo h($contacts->telefone) ?></td>
                <td class="actions">
                    <?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span> Detalhes do Contato', ['action' => 'view', $contacts->id, 'controller'=>'contacts'], ['escape' => false, 'class'=>'btn btn-xs btn-primary']) ?>
                </td>
            </tr>
        <?php
            $i++;
            endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
