<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Contact'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contacts index large-9 medium-8 columns content">
    <h3><?= __('Contatos') ?></h3>
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id') ?></th>
                <th><?php echo $this->Paginator->sort('Grupo Vinculado') ?></th>
                <th><?php echo $this->Paginator->sort('nome') ?></th>
                <th><?php echo $this->Paginator->sort('instituicao') ?></th>
                <th><?php echo $this->Paginator->sort('E-mail') ?></th>
                <th><?php echo $this->Paginator->sort('telefone') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contacts as $contact): ?>
            <tr>
                <td><?php echo $this->Number->format($contact->id) ?></td>
                <td><?php echo $contact->has('group') ? $this->Html->link($contact->group->nome, ['controller' => 'Groups', 'action' => 'view', $contact->group->nome]) : '' ?></td>
                <td><?php echo h($contact->nome) ?></td>
                <td><?php echo h($contact->instituicao) ?></td>
                <td><?php echo h($contact->email) ?></td>
                <td><?php echo h($contact->telefone) ?></td>
                <td class="btn-group">
                    <?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span> Detalhe', ['action' => 'view', $contact->id], ['escape' => false, 'class'=>'btn btn-xs btn-primary']) ?>
                    <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', ['action' => 'edit', $contact->id], ['escape' => false, 'class' => 'btn btn-xs btn-success']) ?>
                    <?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span> Excluir'), ['action' => 'delete', $contact->id], ['class' => 'btn btn-xs btn-danger','escape' => false, 'confirm' => __('Deseja Excluir este Contato # {0}?', $contact->id)] ); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentClass' => 'active')); ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
