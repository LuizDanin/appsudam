<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Contact'), ['action' => 'edit', $contact->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Contact'), ['action' => 'delete', $contact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contact->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Contacts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contact'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<h2>Detalhes do Contato</h2>
<div class="contacts view large-9 medium-8 columns content">
    <h3><?= h($contact->nome) ?></h3>
    <table class="table table-no-border">
        <tr>
            <th><?= __('Grupo:') ?></th>
            <td><?= $contact->has('group') ? $this->Html->link($contact->group->nome, ['controller' => 'Groups', 'action' => 'view', $contact->group->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Nome:') ?></th>
            <td class="col-sm-9"><?= h($contact->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Instituicao:') ?></th>
            <td class=""><?= h($contact->instituicao) ?></td>
        </tr>
        <tr>
            <th><?= __('Email:') ?></th>
            <td><?= h($contact->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Telefone:') ?></th>
            <td><?= h($contact->telefone) ?></td>
        </tr>
    </table>
    <div class="btn btn-group">
	<?php 
		$id = $this->request->params['pass'][0];
		echo $this->Html->link('<span class="glyphicon glyphicon-arrow-left"></span> Voltar',array('action'=>'index'),array('class'=>'btn btn-primary', 'escape' => false)); 
		echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> Editar', array('controller' => 'contacts', 'action' => 'edit' , $id),array('escape' => false, 'class' => 'btn btn-success'));
		echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span> Excluir'), ['action' => 'delete', $id], ['class' => 'btn btn-danger','escape' => false, 'confirm' => __('Deseja Excluir este Contato # {0}?', $id)] );
		
		//echo $this->Form->postLink(__('Delete'), ['action' => 'delete', $id], ['confirm' => __('Deseja Excluir este Contato # {0}?', $id)]);
	?>
</div>
</div>
