<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Contacts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?></li>
    </ul>
</nav>
    <?php echo $this->Form->create($contact, ['align' => [
    'sm' => [
        'left' => 6,
        'middle' => 6,
        'right' => 12
    ],
    'md' => [
        'left' => 4,
        'middle' => 4,
        'right' => 4
    ]
]]) ?>
    <fieldset>
        <legend><?= __('Adicionar Contato') ?></legend>
        <?php
            echo $this->Form->input('nome', ['label'=>'Nome:']);
            echo $this->Form->input('instituicao', ['label'=>'Instituição:']);
            echo $this->Form->input('email', ['label'=>'E-mail:']);
            echo $this->Form->input('telefone', ['label'=>'Telefone:']);
            echo $this->Form->input('group_id', ['label'=>'Grupo', 'options' => $groups]);
            //echo $this->Form->button(__('Submit'));
        ?>
    </fieldset>
<div class="form-group submit">
	<div class="col-sm-offset-6 col-md-offset-4 col-sm-6 col-md-4">
      <?php 
      		echo $this->Form->button('Salvar Contato',array('class'=>'btn btn-primary'));
      		echo $this->Html->link('Cancelar',array('action'=>'index'),array('class'=>'btn btn-danger'));
      ?>
    </div>
</div>
<?php echo $this->Form->end() ?>