-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 25/03/2016 às 18:55
-- Versão do servidor: 5.5.47-0+deb8u1
-- Versão do PHP: 5.6.17-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `contact`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
`id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `instituicao` varchar(150) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `contacts`
--

INSERT INTO `contacts` (`id`, `group_id`, `nome`, `instituicao`, `email`, `telefone`, `created`, `modified`) VALUES
(5, 7, 'teste2', 'ufpa', 'teste2@teste2.com', '999999999', '2016-03-24 23:28:26', '2016-03-24 23:28:26'),
(6, 8, 'teste3', 'ufpa', 'teste3@teste3.com', '32325555', '2016-03-24 23:32:13', '2016-03-24 23:32:13'),
(7, 9, 'teste4', 'ufpa', 'teste4@teste4.com', '32325555', '2016-03-25 00:36:44', '2016-03-25 00:36:44'),
(8, 8, 'Luiz Danin', 'UFPA', 'luiz.danin.5@luiz.com', '0000000000', '2016-03-25 19:44:37', '2016-03-25 19:44:37'),
(9, 8, 'Otavio Lima', 'UFPA', 'otavio@otavio.com', '3333333', '2016-03-25 19:45:21', '2016-03-25 19:45:45'),
(10, 8, 'Danin', 'UFPA', 'danin@danin.com', '545455454', '2016-03-25 19:47:46', '2016-03-25 19:47:46');

-- --------------------------------------------------------

--
-- Estrutura para tabela `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `groups`
--

INSERT INTO `groups` (`id`, `nome`, `created`, `modified`) VALUES
(6, 'Belém', '2016-03-24 03:32:16', '2016-03-25 19:57:45'),
(7, 'Marabá', '2016-03-24 03:47:04', '2016-03-25 19:57:53'),
(8, 'Santarém', '2016-03-24 04:05:53', '2016-03-25 19:58:00'),
(9, 'Manaus', '2016-03-24 05:14:57', '2016-03-25 19:58:08');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `contacts`
--
ALTER TABLE `contacts`
 ADD PRIMARY KEY (`id`), ADD KEY `group_id` (`group_id`);

--
-- Índices de tabela `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `contacts`
--
ALTER TABLE `contacts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de tabela `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `contacts`
--
ALTER TABLE `contacts`
ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
